# Assingments for CS252: July-Dec 2016.


## Assignment 1. (Aug 6, 2016)

All these assignments have to be demonstrated in front of the TA. You
will be asked questions on it.

1. Show the network parameters like IP address, netmask, default
   gateway etc of your cloud container. You need to show these
   parameters on the shell command line using appropriate shell
   commands. It is *not enough* to show it on you admin panel.

2. Find out all networking services are running on your system that
   uses some tcp or udp port.

3. Find the network route, i.e. the intermediate nodes in the network,
   that is taken by a packet sent from here to www.iitb.ac.in.

4. Block access to ssh on your machine form vyom.cc.iitk.ac.in
